import os,json
from natsort import natsorted

from json_create import json_functions
from rooms_functions import rooms_functions_class

class MainFormLogic:
    json_functions_obj = json_functions()

    outer_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
    input_image_path = outer_directory+'/input/images/'
    input_text_path = outer_directory + '/input/text_files/'
    output_path = outer_directory + '/output/'

    # --1. genrate system JSON based system .txt files
    for input_image in natsorted(os.listdir(input_image_path)):
        image_name = str(input_image)[:-4]
        print image_name
        try:
            all_floorplan_components = json_functions_obj.read_text_files(input_text_path,image_name)
            json_functions_obj.add_to_json_file(all_floorplan_components, image_name, output_path+'system_json/')
        except:
            print image_name+ ' this should be exempted'

    # #-- 2. generate room GT JSON based on PNG images(CVC)/ tool drawn JSON(SVG,RSVG)
    # #-- assign text to rooms
    # rooms_functions_obj = rooms_functions_class()
    # # # --2.1 CVC -prepare rooms in CVC by converting room PNG to JSON, adding text label to rooms and writing to same JSON
    # rooms_functions_obj.cvc_room_find(outer_directory+'/input/Room_GTs/',output_path+'gt_json/')

    # --2.1 SVG/RSVG -prepare rooms based on sloth tool drawn JSON files
    # rooms_functions_obj.svg_room_find(outer_directory + '/input/Room_GTs/', output_path+'gt_json/')


if __name__ == '__main__':
    hwl1 = MainFormLogic()