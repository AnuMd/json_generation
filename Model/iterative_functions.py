import numpy as np, cv2, os, shutil
from operator import itemgetter

class iterative_functions_class:
    def __init__(self):
        pass

    def make_directory(self, path, delete_and_write):
        # --if exists delete and remake (folder resets every time)
        if delete_and_write:
            if os.path.exists(path):
                shutil.rmtree(path)
            os.mkdir(path)
        # --else create it only if doesn't exist==don't remake
        else:
            if not (os.path.exists(path)):
                os.mkdir(path)


    # --for one set of contour points return the contour
    def convert_points_to_contour(self, point_list):
        add_bracket_cont_points = []
        for cont_point in point_list:
            add_bracket_cont_points.append([cont_point])
        contour = np.asarray(add_bracket_cont_points)
        return contour

    def get_centre_of_contour(self,contour):
        M = cv2.moments(contour)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        return cx, cy

    def find_contours(self,path):
        gray_image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        ret, thresh = cv2.threshold(gray_image, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        cont_list = []
        for cnt in contours:
            cont_list.append([cnt,cv2.contourArea(cnt)])
        cont_list.sort(key=itemgetter(1))
        return cont_list

    def get_points_from_contour(self,contour):
        contour_point_list = []
        for level1 in contour:
            for level2 in level1:
                x,y = level2
                contour_point_list.append([x,y])
        return contour_point_list


    def find_centre_of_line(self,line):
        p1,p2 = line
        x1,y1 = p1
        x2,y2 = p2
        center_x, center_y= (x2+x1)/2,(y1+y2)/2
        # center_x, center_y= x1+((x2-x1)/2),y1+((y1-y2)/2)
        return center_x, center_y

    def find_points_rectangle(self,x1,y1,x2,y2):
        p1 = [x1,y1]
        p2 = [x2,y1]
        p3 = [x2,y2]
        p4 = [x1,y2]
        return p1, p2, p3, p4

    def get_image_height_width(self,path):
        gray_image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        height, width = gray_image.shape
        return height, width

    def convert_list_elements_to_tuples(self,large_list):
        tuple_list = []
        for row in large_list:
            tuple_list.append(row)

    def find_if_SGL_room(self,text):
        text_word = text.split()
        # --check if the word has two words; 2nd word is len()==1 and is a digit
        # --- if so take only first word ex: chambre 1 ;_ we need only 'chambre'
        if len(text_word) > 1 and text_word[1].isdigit():
            detected_word = text_word[0].replace(' ','')
            # digit_exists = True
        else:
            detected_word = text.replace(' ','')
            # digit_exists = False

        text_files_path = os.getcwd() + '/non_match_rooms.txt'
        file = open(text_files_path, 'r+')
        string = file.read()
        words_list = string.split()

        if detected_word in words_list:
            return False
        else:
            return True