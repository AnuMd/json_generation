import os, cv2, shutil, sys, re, json
import numpy as np
from collections import OrderedDict
from natsort import natsorted

from iterative_functions import iterative_functions_class



class rooms_functions_class():
    def __init__(self):
        self.iterative_functions_obj = iterative_functions_class()

    def cvc_room_find(self,gt_path,output_path):
        room_png_path = gt_path+'Room_PNG/'
        all_content_json_path = gt_path+'JSON/'
        # seperation_png_path = gt_path + 'Room_PNG/'
        # room_output_path = output_path+'rooms/'
        # if os.path.exists(room_output_path):
        #     shutil.rmtree(room_output_path)
        # os.mkdir(room_output_path)


        # self.pre_process_rooms(room_png_path,seperation_png_path,output_path)

        for room_image_file in natsorted(os.listdir(room_png_path)):
            if str(room_image_file)== '.DS_Store':
                continue

            only_image_name = str(room_image_file)[:-4]
            json_file_path = all_content_json_path+only_image_name+'.json'
            json_text_list = self.read_JSON_file(json_file_path)

            json_output_file = open(output_path+only_image_name+'_rooms.json', 'w')
            values = []
            room_image = cv2.imread(room_png_path+str(room_image_file),cv2.IMREAD_GRAYSCALE)
            # height, width = room_image.shape
            ret, thresh = cv2.threshold(room_image, 127, 255, 1)
            room_contours, hierachy = cv2.findContours(thresh, 1, 2)
            for c,room_cnt in enumerate(room_contours):
                if c != len(room_contours)-1:
                    # gt_rooms_img = ~(np.zeros((height, width, 3), np.uint8))
                    # cv2.drawContours(gt_rooms_img, [cnt], -1, (0, 0, 0), -1)
                    # cv2.imwrite(room_output_path + '_'+str(c) + '_gt_rooms.png', gt_rooms_img)
                    # for text_data in json_text_list:
                    #     text = text_data[0]
                    #     is_SGL = self.iterative_functions_obj.find_if_SGL_room(text)
                    #
                    #     if is_SGL and text != 'pl' and text != 'placard' and text !='P':
                    #         text_cx, text_cy = text_data[2]
                    #
                    #         if cv2.pointPolygonTest(cnt,(text_cx,text_cy),False) == 1:
                    #             cnt_point_list = self.iterative_functions_obj.get_points_from_contour(cnt)
                    #             room_row = [cnt_point_list,text,[text_cx, text_cy]]
                    #             values = self.write_to_json(room_row,values)
                    #             break

                    room_point_list = self.iterative_functions_obj.get_points_from_contour(room_cnt)
                    room_text_list = []
                    for text_data in json_text_list:
                        text = text_data[0]
                        if text != 'pl' and text != 'placard' and text != 'p' and text != 'closet' and text != 'cl':
                            text_cx, text_cy = text_data[2]
                            if cv2.pointPolygonTest(room_cnt, (text_cx, text_cy), False) == 1:
                                room_text_list.append([text, [text_cx, text_cy]])

                    if len(room_text_list) > 0:
                        values = self.write_to_json_rooms(room_point_list, room_text_list, values)

            json_values = [OrderedDict([('annotations', values), ('class', 'image'),
                                        ('filename', only_image_name + '.png')])]

            jsonData = json.dumps(json_values, indent=4)
            json_output_file.write(jsonData)
        #
        #     # sys.exit()

    # def pre_process_rooms(self,room_png_path,seperation_png_path,output_path):
    #     for room_image_file in natsorted(os.listdir(room_png_path)):
    #         room_image = cv2.imread(room_png_path + str(room_image_file), cv2.IMREAD_COLOR)
    #         seperation_image = cv2.imread(seperation_png_path + str(room_image_file), cv2.IMREAD_COLOR)
    #         height,width,depth = room_image.shape
    #         contour_OR_img = ~(np.zeros((height, width, 3), np.uint8))
    #         cv2.bitwise_or(room_image, seperation_image, contour_OR_img)
    #
    #         # AND_image = cv2.bitwise_or(room_image, seperation_image, mask=None)
    #         cv2.imwrite(output_path+'OR_image'+room_image_file[:-4]+'.png',contour_OR_img)
    #         pass


    def svg_room_find(self,gt_path,output_path):
        all_content_json_path = gt_path + 'JSON/'
        # room_output_path = output_path + 'rooms/'
        # if os.path.exists(room_output_path):
        #     shutil.rmtree(room_output_path)
        # os.mkdir(room_output_path)

        # self.calculate_rooms(gt_path, room_output_path,all_content_json_path)

        for json_file in natsorted(os.listdir(all_content_json_path)):
            only_image_name = str(json_file)[:-5]
            json_output_file = open(output_path + only_image_name + '_rooms.json', 'w')

            json_rooms_list = self.read_JSON_rooms(all_content_json_path+str(json_file))
            json_text_list = self.read_JSON_file(all_content_json_path+str(json_file))

            values = []
            for each_room in json_rooms_list:
                room_cnt = self.iterative_functions_obj.convert_points_to_contour(each_room)
                room_point_list = self.iterative_functions_obj.get_points_from_contour(room_cnt)
                room_text_list = []
                for text_data in json_text_list:
                    text = text_data[0]
                    if text != 'pl' and text != 'placard' and text != 'p' and text !='closet' and text !='cl':
                        text_cx, text_cy = text_data[2]
                        if cv2.pointPolygonTest(room_cnt, (text_cx, text_cy), False) == 1:
                            room_text_list.append([text, [text_cx, text_cy]])

                if len(room_text_list)>0:
                    values = self.write_to_json_rooms(room_point_list,room_text_list,values)

                            # room_row = [cnt_point_list, text, [text_cx, text_cy]]
                            # values = self.write_to_json(room_row, values)
                            # break

        # for room_image_file in natsorted(os.listdir(room_png_path)):
        #     only_image_name = str(room_image_file)[:-4]
        #
        #     json_file_path = all_content_json_path + only_image_name + '.json'
        #     json_text_list = self.read_JSON_file(json_file_path)
        #
        #     json_output_file = open(all_content_json_path + only_image_name + '_rooms.json', 'w')
        #     values = []
        #     room_image = cv2.imread(room_png_path + str(room_image_file), cv2.IMREAD_GRAYSCALE)
        #     # height, width = room_image.shape
        #     ret, thresh = cv2.threshold(room_image, 127, 255, 1)
        #     room_contours, hierachy = cv2.findContours(thresh, 1, 2)
        #     for c, cnt in enumerate(room_contours):
        #         if c != len(room_contours) - 1:
        #             # gt_rooms_img = ~(np.zeros((height, width, 3), np.uint8))
        #             # cv2.drawContours(gt_rooms_img, [cnt], -1, (0, 0, 0), -1)
        #             # cv2.imwrite(room_output_path + '_'+str(c) + '_gt_rooms.png', gt_rooms_img)
        #             for text_data in json_text_list:
        #                 text = text_data[0]
        #                 is_SGL = self.iterative_functions_obj.find_if_SGL_room(text)
        #
        #                 if is_SGL and text != 'pl' and text != 'placard' and text != 'P':
        #                     text_cx, text_cy = text_data[2]
        #
        #                     if cv2.pointPolygonTest(cnt, (text_cx, text_cy), False) == 1:
        #                         cnt_point_list = self.iterative_functions_obj.get_points_from_contour(cnt)
        #                         room_row = [cnt_point_list, text, [text_cx, text_cy]]
        #                         values = self.write_to_json(room_row, values)
        #                         break

            json_values = [OrderedDict([('annotations', values), ('class', 'image'),
                                        ('filename', only_image_name + '.png')])]

            jsonData = json.dumps(json_values, indent=4)
            json_output_file.write(jsonData)

    def read_JSON_rooms(self,json_path):
        rooms_list = []
        with open(json_path) as json_file:
            data = json.load(json_file)
            for all_data in data:
                element_value = all_data['annotations']
                for element in element_value:
                    if element['class'] == 'Room':
                        # --get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get room cont cords
                        room_cords = []
                        for x_num, x in enumerate(int_x_list):
                            room_cords.append([x, int_y_list[x_num]])
                        rooms_list.append(room_cords)

                    elif element['class'] == 'room':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x, top_left_y,top_left_x + width,top_left_y + height)
                        room_cords = [p1, p2, p3, p4]
                        rooms_list.append(room_cords)
        return rooms_list

    def read_JSON_file(self, path):
        self.iterative_functions_obj = iterative_functions_class()
        json_text_list = []
        with open(path) as json_file:
            data = json.load(json_file)
            for all_data in data:
                element_value = all_data['annotations']
                for element in element_value:
                    if element['class'] == 'Text':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        text = str(element['text'])
                        bottom_right_x, bottom_right_y = top_left_x + width, top_left_y + height
                        center_x, center_y = self.iterative_functions_obj.find_centre_of_line(
                            [[top_left_x, top_left_y], [bottom_right_x, bottom_right_y]])
                        # # --add to text_list
                        json_text_list.append([text, height, [center_x, center_y],
                                          [top_left_x, top_left_y, bottom_right_x, bottom_right_y]])

        return json_text_list

    def write_to_json(self,room_row,values):
        room_point_list, text, centre_p = room_row
        xs = [row[0] for row in room_point_list]
        ys = [row[1] for row in room_point_list]
        xn_string, yn_string = '', ''
        for x in xs:
            xn_string = xn_string + str(x) + ';'
        for y in ys:
            yn_string = yn_string + str(y) + ';'
        values.append(OrderedDict([('class', 'Room'),
                                   ('type','UN'),
                                   ('xn', xn_string[:-1]),
                                   ('yn', yn_string[:-1]),
                                   ('text', text),
                                   ('x', centre_p[0]),
                                   ('y', centre_p[1])]))

        return values

    def write_to_json_rooms(self, room_point_list,text_list,values):
        text_string, text_x_string, text_y_string = '','',''
        if len(text_list) == 1:
            room_type = 'single'
        else:
            room_type = 'open'

        for room_text in text_list:
            text, text_cords = room_text
            text_string = text_string + text + ';'
            text_x_string = text_x_string + str(text_cords[0]) + ';'
            text_y_string = text_y_string + str(text_cords[1]) + ';'

        xs = [row[0] for row in room_point_list]
        ys = [row[1] for row in room_point_list]
        xn_string, yn_string = '', ''
        for x in xs:
            xn_string = xn_string + str(x) + ';'
        for y in ys:
            yn_string = yn_string + str(y) + ';'
        values.append(OrderedDict([('class', 'Room'),
                                   ('type', room_type),
                                   ('xn', xn_string[:-1]),
                                   ('yn', yn_string[:-1]),
                                   ('text', text_string),
                                   ('x', text_x_string[:-1]),
                                   ('y', text_y_string[:-1])]))

        return values

    def calculate_rooms(self,gt_path,room_output_path,all_content_json_path):
        wall_path = gt_path+'Wall/'
        door_path = gt_path+'Door/'
        window_path = gt_path+'Window/'
        for room_image_file in natsorted(os.listdir(wall_path)):
            only_image_name = str(room_image_file)[:-4]
            wall_image = cv2.imread(wall_path + str(room_image_file), cv2.IMREAD_COLOR)
            height, width, depth = wall_image.shape
            door_image = cv2.imread(door_path + str(room_image_file), cv2.IMREAD_COLOR)
            window_image = cv2.imread(window_path+str(room_image_file),cv2.IMREAD_COLOR)

            contour_OR_img_v1 = ~(np.zeros((height, width, 3), np.uint8))
            cv2.bitwise_or(wall_image, door_image, contour_OR_img_v1, mask=None)
            contour_OR_img_v2 = ~(np.zeros((height, width, 3), np.uint8))
            cv2.bitwise_or(contour_OR_img_v1,window_image,contour_OR_img_v2,mask=None)

            # cv2.imwrite(room_output_path + '_all.png', contour_OR_img_v2)

            kernel = np.ones((8, 8), np.uint8)
            all_detections = cv2.dilate(contour_OR_img_v2, kernel, iterations=1)
            cv2.imwrite(room_output_path + '_'+ only_image_name+'_dilation.png', all_detections)
            all_detections_inv = cv2.bitwise_not(all_detections)

            json_file_path = all_content_json_path+only_image_name+'.json'
            json_text_list = self.read_JSON_file(json_file_path)

            matched_rooms = []
            gray_all_detections = cv2.cvtColor(all_detections_inv, cv2.COLOR_BGR2GRAY)
            ret, thresh = cv2.threshold(gray_all_detections, 127, 255, 1)
            room_contours, hierachy = cv2.findContours(thresh, 1, 2)
            for c,cnt in enumerate(room_contours):
                if c != len(room_contours)-1:
                    room_match = []
                    for text_data in json_text_list:
                        text = text_data[0]
                        text_cx, text_cy = text_data[2]
                        if cv2.pointPolygonTest(cnt,(text_cx,text_cy),False) == 1:
                            room_match.append([text, [text_cx, text_cy]])

                    if len(room_match) == 1:
                        cnt_point_list = self.iterative_functions_obj. get_points_from_contour(cnt)
                        text, centre_text = room_match[0]
                        matched_rooms.append([c,cnt_point_list,text,centre_text])


            json_output_file = open(all_content_json_path+only_image_name+ '_rooms.json', 'w')
            values = []
            for row in matched_rooms:
                c, cnt_point_list, text, centre_text = row
                values = self.write_to_json([cnt_point_list, text, centre_text], values)

                # gt_rooms_img = ~(np.zeros((height, width, 3), np.uint8))
                # cnt = self.iterative_functions_obj.convert_points_to_contour(cnt_point_list)
                # text_cx, text_cy = centre_text
                # cv2.drawContours(gt_rooms_img, [cnt], -1, (0, 0, 0), -1)
                # cv2.putText(gt_rooms_img, text.upper(), (text_cx - 10, text_cy - 30), cv2.FONT_HERSHEY_COMPLEX, 1,(0, 0, 255), 3, cv2.CV_AA)
                # cv2.imwrite(room_output_path + '_' + str(c) + '_gt_rooms.png', gt_rooms_img)

            json_values = [OrderedDict([('annotations', values), ('class', 'image'),
                                        ('filename', only_image_name + '.png')])]

            jsonData = json.dumps(json_values, indent=4)
            json_output_file.write(jsonData)

            # sys.exit()

    def generate_room_door_window_image(self,gt_path,input_image_path,room_output_path):
        for json_file in natsorted(os.listdir(gt_path)):
            only_image_name = str(json_file)[:-5]
            gt_json_data = self.read_JSON_file_full(gt_path + str(json_file))

            fp_image = cv2.imread(input_image_path+only_image_name+'.png',cv2.IMREAD_COLOR)
            height, width, depth = fp_image.shape

            self.add_to_image(gt_json_data,height, width,room_output_path+only_image_name)

    def read_JSON_file_full(self, path):
        self.iterative_functions_obj = iterative_functions_class()
        json_data = {}
        rooms_list, wall_list, door_list,window_list,stair_list,text_list,unknown_list = [],[],[],[],[],[],[]
        with open(path) as json_file:
            data = json.load(json_file)
            for all_data in data:
                element_value = all_data['annotations']
                for element in element_value:
                    if element['class'] == 'Room':
                        # --get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get room cont cords
                        room_cords = []
                        for x_num, x in enumerate(int_x_list):
                            room_cords.append([x, int_y_list[x_num]])
                        room_type = str(element['type'])
                        text = str(element['text'])
                        text_centre_x = int(float(element['x']))
                        text_centre_y = int(float(element['y']))
                        # --add to wall list
                        rooms_list.append([text,[text_centre_x,text_centre_y],room_type,room_cords])


                    if element['class']=='Wall':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get wall cords
                        wall_cords = []
                        for x_num, x in enumerate(int_x_list):
                            wall_cords.append([x,int_y_list[x_num]])
                        # --add to wall list
                        wall_list.append(wall_cords)
                    elif element['class']=='wall':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        wall_cords = [p1, p2, p3, p4]
                        # --add to wall list
                        wall_list.append(wall_cords)

                    elif element['class']=='Door':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get door cords
                        door_cords = []
                        for x_num, x in enumerate(int_x_list):
                            door_cords.append([x,int_y_list[x_num]])
                        # --add to door list
                        door_list.append(door_cords)

                    elif element['class']=='door':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        door_cords = [p1, p2, p3, p4]
                        # --add to wall list
                        door_list.append(door_cords)

                    elif element['class']=='Window':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get window cords
                        window_cords = []
                        for x_num, x in enumerate(int_x_list):
                            window_cords.append([x,int_y_list[x_num]])
                        # --add to window list
                        window_list.append(window_cords)

                    elif element['class']=='window':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        window_cords = [p1, p2, p3, p4]
                        # --add to wall list
                        window_list.append(window_cords)

                    elif element['class']=='Stair' or element['class']=='Stairs':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get stair cords
                        stair_cords = []
                        for x_num, x in enumerate(int_x_list):
                            stair_cords.append([x,int_y_list[x_num]])
                        # --add to stair list
                        stair_list.append(stair_cords)

                    elif element['class']=='Text':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        text = str(element['text'])
                        bottom_right_x, bottom_right_y = top_left_x+width, top_left_y+height
                        center_x, center_y = self.iterative_functions_obj.find_centre_of_line([[top_left_x,top_left_y], [bottom_right_x, bottom_right_y]])
                        #--use no numbers for evaluation
                        text_new = ''.join(x for x in text if x.isalpha())
                        if 'floor' in text_new:
                            continue
                        # # --add to text_list
                        text_list.append([text_new,height,[center_x, center_y],[top_left_x,top_left_y, bottom_right_x, bottom_right_y]])

                    elif element['class']=='Unknown':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get window cords
                        unknown_cords = []
                        for x_num, x in enumerate(int_x_list):
                            unknown_cords.append([x,int_y_list[x_num]])
                        # --add to window list
                        unknown_list.append(unknown_cords)

        json_data['rooms_list'] = rooms_list
        json_data['wall_list'] = wall_list
        json_data['door_list'] = door_list
        json_data['window_list'] = window_list
        json_data['stair_list'] = stair_list
        json_data['text_list'] = text_list
        json_data['unknown_list'] = unknown_list

        return json_data

    def add_to_image(self,json_data,height, width,room_output_path):
        wall_list = json_data['wall_list']
        door_list = json_data['door_list']
        window_list = json_data['window_list']
        stair_list = json_data['stair_list']
        text_list = json_data['text_list']

        gt_rooms_img = (np.zeros((height, width, 3), np.uint8))
        for wall in wall_list:
            wall_cnt = self.iterative_functions_obj.convert_points_to_contour(wall)
            cv2.drawContours(gt_rooms_img, [wall_cnt], -1, (255, 255, 255), -1)
        # cv2.imwrite(room_output_path + '_gt_rooms_1.png', gt_rooms_img)

        kernel = np.ones((8, 8), np.uint8)
        gt_rooms_img_walls = cv2.dilate(gt_rooms_img, kernel, iterations=1)

        for points in door_list+window_list+stair_list:
            cnt = self.iterative_functions_obj.convert_points_to_contour(points)
            cv2.drawContours(gt_rooms_img_walls, [cnt], -1, (255, 255, 255), -1)

        kernel = np.ones((12, 12), np.uint8)
        gt_rooms_img_2 = cv2.dilate(gt_rooms_img_walls, kernel, iterations=1)

        for row in text_list:
            text,centre = row[0],row[2]
            text_cx,text_cy = centre
            cv2.putText(gt_rooms_img_2, text.upper(), (text_cx - 10, text_cy - 30), cv2.FONT_HERSHEY_COMPLEX, 1,(0, 0, 255), 3, cv2.CV_AA)
        cv2.imwrite(room_output_path + '_gt_rooms_2.png', gt_rooms_img_2)
        # sys.exit()