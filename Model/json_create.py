import os,re,json, numpy as np
from natsort import natsorted
from collections import OrderedDict
from iterative_functions import iterative_functions_class



class json_functions:

    def __init__(self):
        self.iterative_functions_obj = iterative_functions_class()


    def read_text_files(self,text_path, only_image_name):
        rooms_point_list = self.read_room_detail_file(text_path,only_image_name)
        stairs_point_list = self.read_stairs_detail_file(text_path, only_image_name)
        windows_point_list = self.read_windows_detail_file(text_path, only_image_name)
        doors_rect_list = self.read_doors_detail_file(text_path, only_image_name)
        wall_point_list = self.read_walls_detail_file(text_path, only_image_name)
        text_data_list = self.read_text_detail_file(text_path, only_image_name)
        unknown_point_list = self.read_unknown_detail_file(text_path, only_image_name)

        # rooms_point_list, stairs_point_list, windows_point_list, doors_rect_list, wall_point_list = [], [], [], [], []
        # rooms_point_list = []

        all_floorplan_components = [rooms_point_list,
                                    stairs_point_list, windows_point_list,
                                    doors_rect_list, wall_point_list,
                                    text_data_list,unknown_point_list]
        return all_floorplan_components

    def read_room_detail_file(self, path_gravvitas, only_image_name):
        # ----read all rooms data
        rooms_point_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_1_room_details.txt'):
            room_details_file = open(path_gravvitas + only_image_name + '_1_room_details.txt', 'r')
            for row_num, text_line in enumerate(room_details_file):
                line_seperate = text_line.split(':')
                if len(line_seperate) > 3:
                    # ---get room contour points
                    contour_details = re.findall(r"[\w']+", line_seperate[5])
                    int_contour_details = [int(x) for x in contour_details]
                    room_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    # ---get room text
                    room_text = (line_seperate[2].strip()).replace('maybe ','')
                    # --seperate and convert text cordinate to int
                    cordinates_string_2 = re.findall(r"[\w.']+", line_seperate[3])
                    int_centre_p_cordinates = [int(float(x)) for x in cordinates_string_2]
                    # centre_point_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]
                    # ---get room type details
                    # room_type_str = line_seperate[1].strip()
                    # if '-' in room_type_str:
                    #     room_type = (room_type_str.split('-'))[0].strip()
                    # else:
                    #     room_type = 'single'

                    # is_SGL = self.iterative_functions_obj.find_if_SGL_room(room_text)
                    # if is_SGL:
                    #     rooms_point_list.append([room_text, int_centre_p_cordinates, 'single', room_contour])
                    if room_text != 'pl' and room_text != 'placard' and room_text != 'p' and room_text !='closet' and room_text !='cl':
                        rooms_point_list.append([room_text, int_centre_p_cordinates, line_seperate[0].strip(), room_contour])

            # # -- get open plan data in teh same format as rooms
            # all_op_data = self.read_open_plan_file(path_gravvitas, only_image_name)
            # for op_row in all_op_data:
            #     op_number, op_room_list = op_row
            #     for each_op_room in op_room_list:
            #         room_name, text_cord, room_cnt = each_op_room
            #         is_SGL = self.iterative_functions_obj.find_if_SGL_room(room_name)
            #
            #         if is_SGL:
            #             rooms_point_list.append([room_name, text_cord, 'single', room_cnt])

        return rooms_point_list

    def read_open_plan_file(self, path_gravvitas, only_image_name):
        all_op_data = []
        if os.path.exists(path_gravvitas + only_image_name + '_8_openPlan_details.txt'):
            room_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'r')
            for op_number, text_line in enumerate(room_details_file):
                contour_seperate = text_line.split(':')
                if len(contour_seperate) > 3:
                    all_room_data = contour_seperate[5:]
                    room_data_list = [all_room_data[i:i + 4] for i in range(0, len(all_room_data), 4)]
                    op_room_data = []
                    for row in room_data_list:
                        # --extract text label and text codinate
                        text_label = row[0].strip()
                        if 'maybe' in text_label:
                            new_text_label = text_label.replace('maybe ', '')
                            print 'maybe was in. Resetting ', text_label, ' to ', new_text_label
                        else:
                            new_text_label = text_label
                        text_cord_details = re.findall(r"[\w']+", row[1])
                        text_cord = [int(x) for x in text_cord_details]
                        # ---extract room contour
                        contour_details = re.findall(r"[\w']+", row[3])
                        int_contour_details = [int(x) for x in contour_details]
                        chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                        add_bracket = []
                        for each_chunk in chunks:
                            add_bracket.append([each_chunk])
                        room_contour = np.asarray(add_bracket)
                        op_room_data.append([new_text_label, text_cord, room_contour])
                        # print 'text_label,text_cord',text_label,text_cord
                    all_op_data.append([op_number, op_room_data])
        return all_op_data

    def read_object_detail_file(self, path_gravvitas, only_image_name):
        object_point_list = []
        object_text_file_path = path_gravvitas + only_image_name + '_2_object_details.txt'
        if os.path.exists(object_text_file_path):
            object_details_file = open(path_gravvitas + only_image_name + '_2_object_details.txt', 'r')
            for text_line in object_details_file:
                line_seperate = text_line.split(':')
                if len(line_seperate) > 1:
                    object_contour = re.findall(r"[\w']+", line_seperate[1])
                    int_object_details = [int(x) for x in object_contour]
                    object_chunks = [int_object_details[x:x + 2] for x in xrange(0, len(int_object_details), 2)]
                    # ---temp fix delete later
                    x1, y1 = object_chunks[0]
                    x2, y2 = object_chunks[1]
                    object_contour_new = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]

                    # --get object_name
                    object_name = line_seperate[0].split()[0]
                    # print object_name
                    if len(object_point_list) == 0:
                        object_point_list.append([])
                        object_point_list[0].append(object_name)
                        object_point_list[0].append(object_contour_new)
                    else:
                        row_ID = -1
                        for obj_row, each_object in enumerate(object_point_list):
                            if each_object[0] == object_name:
                                row_ID = obj_row
                                break
                        if row_ID != -1:
                            object_point_list[row_ID].append(object_contour_new)
                        else:
                            object_point_list.append([])
                            object_point_list[len(object_point_list) - 1].append(object_name)
                            object_point_list[len(object_point_list) - 1].append(object_contour_new)
        return object_point_list

    def read_stairs_detail_file(self, path_gravvitas, only_image_name):
        stairs_point_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_3_stairs_details.txt'):
            stair_details_file = open(path_gravvitas + only_image_name + '_3_stairs_details.txt', 'r')
            for stair_num, text_line in enumerate(stair_details_file):
                line_seperate = text_line.split(':')
                if len(line_seperate) > 2:
                    contour_details = re.findall(r"[\w']+", line_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    stair_points = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    # --get confidence
                    confidence_string = re.findall(r"[\w'.]+", line_seperate[2])
                    confidence = float(confidence_string[0])

                    stairs_point_list.append([stair_points,confidence])
        return stairs_point_list

    def read_windows_detail_file(self, path_gravvitas, only_image_name):
        windows_point_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_4_window_details.txt'):
            window_details_file = open(path_gravvitas + only_image_name + '_4_window_details.txt', 'r')
            for win_num, text_line in enumerate(window_details_file):
                contour_seperate = text_line.split(':')
                if len(contour_seperate) > 2:
                    contour_details = re.findall(r"[\w']+", contour_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    window_data = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    # --get confidence
                    confidence_string = re.findall(r"[\w'.]+", contour_seperate[2])
                    confidence = float(confidence_string[0])

                    windows_point_list.append([window_data,confidence])
        return windows_point_list

    def read_doors_detail_file(self, path_gravvitas, only_image_name):
        doors_rect_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_5_door_details.txt'):
            door_details_file = open(path_gravvitas + only_image_name + '_5_door_details.txt', 'r')
            for door_num, text_line in enumerate(door_details_file):
                line_seperate = text_line.split(':')
                if len(line_seperate) > 2:
                    #--- get cordinates
                    contour_details = re.findall(r"[\w']+", line_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    door_rectangle = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    #--get confidence
                    confidence_string = re.findall(r"[\w'.]+",  line_seperate[2])
                    confidence = float(confidence_string[0])

                    #--add to list
                    doors_rect_list.append([door_rectangle,confidence])
        return doors_rect_list

    def read_walls_detail_file(self, path_gravvitas, only_image_name):
        wall_point_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_7_wall_details.txt'):
            wall_details_file = open(path_gravvitas + only_image_name + '_7_wall_details.txt', 'r')
            for wall_num, text_line in enumerate(wall_details_file):
                line_seperate = text_line.split(':')
                if len(line_seperate) > 2:
                    contour_details = re.findall(r"[\w']+", line_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    wall_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    # --get confidence
                    confidence_string = re.findall(r"[\w'.]+", line_seperate[2])
                    confidence = float(confidence_string[0])

                    wall_point_list.append([wall_contour,confidence])
        return wall_point_list

    def read_text_detail_file(self, path_gravvitas, only_image_name):
        text_data_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_6_text_details.txt'):
            text_details_file = open(path_gravvitas + only_image_name + '_6_text_details.txt', 'r')
            for text_num, text_line in enumerate(text_details_file):
                detail_seperate = text_line.split(':')
                if len(detail_seperate) > 1:
                    word, centre_point, bb_data, confidence_data = detail_seperate[0].strip(), detail_seperate[1], detail_seperate[2], detail_seperate[4]

                    # --seperate and convert text cordinate to int
                    cordinates_string_2 = re.findall(r"[\w.']+", centre_point)
                    int_cordinates = [int(float(x)) for x in cordinates_string_2]
                    centre_point_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]

                    # --seperate and convert BB-box to int
                    bb_cordinates_string_2 = re.findall(r"[\w.']+", bb_data)
                    int_bb_cordinates = [int(float(x)) for x in bb_cordinates_string_2]
                    bb_cordintes_chunks = [int_bb_cordinates[x:x + 4] for x in xrange(0, len(int_bb_cordinates), 4)]

                    # --get confidence
                    confidence_string = re.findall(r"[\w']+", confidence_data)
                    confidence = float(confidence_string[0])

                    text_data_list.append([word, centre_point_chunks, bb_cordintes_chunks,confidence])
        return text_data_list

    def read_unknown_detail_file(self,path_gravvitas,only_image_name):
        unknown_point_list = []
        if os.path.exists(path_gravvitas + only_image_name + '_9_unknown_details.txt'):
            unknown_details_file = open(path_gravvitas + only_image_name + '_9_unknown_details.txt', 'r')
            for text_line in unknown_details_file:
                line_seperate = text_line.split(':')
                if len(line_seperate) > 2:
                    contour_details = re.findall(r"[\w']+", line_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    wall_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    unknown_point_list.append(wall_contour)
        return unknown_point_list


    def add_to_json_file(self,all_floorplan_components,image_name,output_path):
        rooms_point_list, stairs_point_list, windows_point_list,doors_rect_list, wall_point_list,text_data_list,unknown_point_list = all_floorplan_components

        json_file = open(output_path + image_name + '.json', 'w')
        values = []

        for room_row in rooms_point_list:
            text, centre_point, room_type, room_point_list = room_row
            xs = [row[0] for row in room_point_list]
            ys = [row[1] for row in room_point_list]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Room'),
                                       ('type', room_type),
                                       ('xn', xn_string[:-1]),
                                       ('yn', yn_string[:-1]),
                                       ('text', text.replace('maybe ','')),
                                       ('x', centre_point[0]),
                                       ('y', centre_point[1])]))

        #--doors
        for door_row in doors_rect_list:
            door_cords, confidence = door_row
            xs = [row[0] for row in door_cords]
            ys = [row[1] for row in door_cords]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Door'),
                                               ('xn', xn_string[:-1]),
                                               ('yn', yn_string[:-1]),
                                                ('confidence',confidence)]))

        #--windows
        for win_row in windows_point_list:
            win_cords, confidence = win_row
            xs = [row[0] for row in win_cords]
            ys = [row[1] for row in win_cords]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Window'),
                                        ('xn', xn_string[:-1]),
                                        ('yn', yn_string[:-1]),
                                        ('confidence',confidence)]))

        #--walls
        for wall_row in wall_point_list:
            wall_cords, confidence = wall_row
            xs = [row[0] for row in wall_cords]
            ys = [row[1] for row in wall_cords]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Wall'),
                                       ('xn', xn_string[:-1]),
                                       ('yn', yn_string[:-1]),
                                       ('confidence',confidence)]))

        # --stairs
        for stair_row in stairs_point_list:
            stair_cords, confidence = stair_row
            xs = [row[0] for row in stair_cords]
            ys = [row[1] for row in stair_cords]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Stair'),
                                       ('xn', xn_string[:-1]),
                                       ('yn', yn_string[:-1]),
                                       ('confidence', confidence)]))

        # --text
        for text_row in text_data_list:
            text,centre_p,bbox, confidence = text_row
            bbox_values = bbox[0]
            xs = [bbox_values[0],bbox_values[2]]
            ys = [bbox_values[1],bbox_values[3]]
            min_x,min_y = min(xs),min(ys)
            width = abs(bbox_values[0] - bbox_values[2])
            height = abs(bbox_values[1] - bbox_values[3])
            confidence_int = round((confidence/float(100)),2)
            values.append(OrderedDict([('class', 'Text'),
                                       ('height', height),
                                       ('width', width),
                                       ('x',min_x),
                                       ('y',min_y),
                                       ('text',text),
                                       ('confidence',str(confidence_int))]))

        # --unknown elements
        for unknown_row in unknown_point_list:
            xs = [row[0] for row in unknown_row]
            ys = [row[1] for row in unknown_row]
            xn_string, yn_string = '', ''
            for x in xs:
                xn_string = xn_string + str(x) + ';'
            for y in ys:
                yn_string = yn_string + str(y) + ';'
            values.append(OrderedDict([('class', 'Unknown'),
                                       ('xn', xn_string[:-1]),
                                       ('yn', yn_string[:-1])]))


        json_values = [OrderedDict([('annotations', values),('class','image'),
                                    ('filename',image_name+'.png')])]

        jsonData = json.dumps(json_values, indent=4)
        json_file.write(jsonData)
